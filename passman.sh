#/bin/sh
#dmenu
#$MENUS
PASS=$(echo "" |$MENUS -fn 'Webdings' -nb black -nf black -p ds) #get the pass
[ "$PASS" = "" ] && exit #terminate if pass not provided

#Let's make a menu for accounts
ACCOUNT=$(gpg -d --batch --passphrase $PASS ~/.config/passman/passwords.xz.gpg |xzcat |cut -f 1 |sort |$MENUS -i -p Account)
[ "$ACCOUNT" = "" ] && exit

#the logic bomb
#xclip -selection clipboard -o > /tmp/clip
wl-paste > /tmp/clip

#gpg -d --batch --passphrase $PASS ~/.config/passman/pässwärds.xz.gpg |xzcat |grep -m1 "$ACCOUNT" |cut -f 2 |xclip -selection clipboard
gpg -d --batch --passphrase $PASS ~/.config/passman/passwords.xz.gpg |xzcat |grep -m1 "$ACCOUNT" |cut -f 2 |wl-copy
notify-send "<b>Username copied</b>"
sleep 5s

#gpg -d --batch --passphrase $PASS ~/.config/passman/pässwärds.xz.gpg |xzcat |grep -m1 "$ACCOUNT" |cut -f 3 |xclip -selection clipboard
gpg -d --batch --passphrase $PASS ~/.config/passman/passwords.xz.gpg |xzcat |grep -m1 "$ACCOUNT" |cut -f 3 |wl-copy
notify-send "<b>Password copied</b>"
sleep 5s

#cat /tmp/clip |xclip -selection clipboard &
cat /tmp/clip |wl-copy
notify-send "<b>Clipboard restored</b>" &
rm /tmp/clip
