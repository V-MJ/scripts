#/bin/sh
MONTH=$(date "+%m")
MONTDATE=$(date "+%m%d")

#add the required paths to a temporary file
#spring
[ "$MONTH" = "03" ] && find ~/Pictures/wallpapers/spring -type f > /tmp/wallpaperlist
[ "$MONTH" = "04" ] && find ~/Pictures/wallpapers/spring -type f > /tmp/wallpaperlist
[ "$MONTH" = "05" ] && find ~/Pictures/wallpapers/spring -type f > /tmp/wallpaperlist

#summer
[ "$MONTH" = "06" ] && find ~/Pictures/wallpapers/summer -type f > /tmp/wallpaperlist
[ "$MONTH" = "07" ] && find ~/Pictures/wallpapers/summer -type f > /tmp/wallpaperlist
[ "$MONTH" = "08" ] && find ~/Pictures/wallpapers/summer -type f > /tmp/wallpaperlist

#auntum includes halloween
[ "$MONTH" = "09" ] && find ~/Pictures/wallpapers/auntum -type f > /tmp/wallpaperlist
#[ "$MONTH" = "10" ] && find ~/Pictures/wallpapers/auntum -type f > /tmp/wallpaperlist
[ "$MONTH" = "11" ] && find ~/Pictures/wallpapers/auntum -type f > /tmp/wallpaperlist

#winter
#[ "$MONTH" = "12" ] && find ~/Pictures/wallpapers/winter -type f > /tmp/wallpaperlist		
[ "$MONTH" = "01" ] && find ~/Pictures/wallpapers/winter -type f > /tmp/wallpaperlist
[ "$MONTH" = "02" ] && find ~/Pictures/wallpapers/winter -type f > /tmp/wallpaperlist

find ~/Pictures/wallpapers/common -type f >> /tmp/wallpaperlist


#event aditions
#school
[ "$MONTH" = "08" ] && find ~/Pictures/wallpapers/events/school -type f >> /tmp/wallpaperlist
[ "$MONTH" = "09" ] && find ~/Pictures/wallpapers/events/school -type f >> /tmp/wallpaperlist
[ "$MONTH" = "10" ] && find ~/Pictures/wallpapers/events/school -type f >> /tmp/wallpaperlist
[ "$MONTH" = "11" ] && find ~/Pictures/wallpapers/events/school -type f >> /tmp/wallpaperlist
[ "$MONTH" = "12" ] && find ~/Pictures/wallpapers/events/school -type f >> /tmp/wallpaperlist
[ "$MONTH" = "01" ] && find ~/Pictures/wallpapers/events/school -type f >> /tmp/wallpaperlist
[ "$MONTH" = "02" ] && find ~/Pictures/wallpapers/events/school -type f >> /tmp/wallpaperlist
[ "$MONTH" = "03" ] && find ~/Pictures/wallpapers/events/school -type f >> /tmp/wallpaperlist
[ "$MONTH" = "04" ] && find ~/Pictures/wallpapers/events/school -type f >> /tmp/wallpaperlist
[ "$MONTH" = "05" ] && find ~/Pictures/wallpapers/events/school -type f >> /tmp/wallpaperlist
#Terry a davis
[ "$MONTHDATE" = "0811" ] && find ~/Pictures/wallpapers/events/terrys_memorial_day -type f > /tmp/wallpaperlist
#halloween
[ "$MONTH" = "10" ] && find ~/Pictures/wallpapers/events/halloween -type f > /tmp/wallpaperlist
#chrismas
[ "$MONTH" = "12" ] && find ~/Pictures/wallpapers/events/chrismas -type f > /tmp/wallpaperlist

#grep -vE "(nsfw)" /tmp/wallpaperlist |shuf -n 1 |xargs -n 1 -I % cp % /tmp/wallpaper && wal -i /tmp/wallpaper && xwallpaper --zoom /tmp/wallpaper
#shuf -n 1 /tmp/wallpaperlist |xargs -n 1 -I % cp % /tmp/wallpaper && wal -i /tmp/wallpaper && xwallpaper --zoom /tmp/wallpaper
shuf -n 1 /tmp/wallpaperlist |xargs -n 1 -I % cp % /tmp/wallpaper && xwallpaper --zoom /tmp/wallpaper

rm /tmp/wallpaperlist
rm /tmp/wallpaper
