#/bin/sh
#software dependencied -> cdparanoia, flac, imagemagick
################
##### HELP #####
################
#insert cd
#run the script in a directory that makes sense like in /tmp/ripityrap
#this is just a assisted ripping program and most likely very buggy
################

#make the dir for tmp files
mkdir -p /tmp/flackrip

#Get the rip
cdparanoia -vwBz || exit
#convert the rip to tiny flacs <- verified to work
for wav in *.wav;
do
	flac -f -V --lax -m --replay-gain --best -e "$wav" || exit #best compression and loslessity !!can stream!!
done;
rm *.wav


#begin perfile foop
for song in *.flac;
do
	##### flac/ogg comments #####
	echo "TITLE=\nVERSION=\nALBUM=\nTRACKNUMBER=\nARTIST=\nPERFORMER=\nCOPYRIGHT=\nLICENSE=\nORGANIZATION=\nDESCRIPTION=\nGENRE=\nDATE=\nLOCATION=\nCONTACT=\nISRC=" >> /tmp/flackrip/song.tags
	$EDITOR /tmp/flackrip/song.tags

	##### image metadata "flac" #####
	echo "OTHER=\nICON=\nCOVERFRONT=\nCOVERBACK=\nLEAFLET=\nSIDELABLE=\nLEAD_ARTIST=\nARTIST=\nCONDUCTOR=\nBAND_ORCHESTRA=\nCOMPOSER=\nLYRICIST=\nRECORDING_LOCATION=\nDURING_RECORDING=\nDURING_PERFORMANCE=\nVIDEO_SCREENSHOT=\nA_BRIGHTLY_COLOURED_FISH=\nILLUSTRATION=\nBAND_LOGOTYPE=\nPUBLISHER_LOGOTYPE=" >> /tmp/flackrip/song.pics
	$EDITOR /tmp/flackrip/song.pics

	#convert song.pics to a format more easily processed
	cat /tmp/flackrip/song.pics |sed 's/=/\t/g;s/OTHER/0/g;s/ICON/2/g;s/COVERFRONT/3/g;s/COVERBACK/4/g;s/LEAFLET/5/g;s/SIDELABLE/6/g;s/LEAD_ARTIST/7/g;s/ARTIST/8/g;s/CONDUCTOR/9/g;s/BAND_ORCHESTRA/10/g;s/COMPOSER/11/g;s/LYRICIST/12/g;s/RECORDING_LOCATION/13/g;s/DURING_RECORDING/14/g;s/DURING_PERFORMANCE/15/g;s/VIDEO_SCREENSHOT/16/g;s/A_BRIGHTLY_COLOURED_FISH/17/g;s/ILLUSTRATION/18/g;s/BAND_LOGOTYPE/19/g;s/PUBLISHER_LOGOTYPE/20/g' > /tmp/flackrip/song.pics

	#compress pngs and add them to the flac
	filename="/tmp/flackrip/song.pics"
	while read line; do
		ptype=$(echo "$line" |cut -f 1) #picture type
		pfile=$(echo "$line" |cut -f 2) #picture file
		metaflac --import-picture-from="$pfile|$ptype" $song || exit
	done < $filename

	#add rest of the matdata also remove exstra padding
	metaflac --import-tags-from="/tmp/flackrip/song.tags" $song || exit
	metaflac --sort-padding $song
	metaflac --merge-padding $song

	#rename the track to a real name and move that shit to a directory named after the albun
	#prolly works
	ndm=$(grep 'ALBUM=*' /tmp/flackrip/$song.tags |sed 's/.*=//g;s/ /_/g')
	mkdir -p $ndm || exit
	newname=$(grep 'TITLE=*' /tmp/flackrip/$song.tags |sed 's/.*=//g;s/ /_/g')
	mv $song $ndm/$newname.flac

#end perfile loop
done;


#The script to rip cd audio and add most of the metadata one could want to add
#This is not a full ripper program.
#You propably can't combine the files created by this program to make a identical cd <- no que file
#I have prioritized digital audio, quality and size when making this program
#Digital audio meaning getting the raw bits form the disc and not abusing the analog hole
#Quality meaning little to no loss from the cd source
#Size meaning as small as possible with no loss of original quality
