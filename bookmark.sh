#/bin/sh
#demenu
#$MENUS
CATEGORY=$(ls ~/scripts/assets/bookmarks/ |sort |$MENUS -i -l 25 -p 'category:')
# [ $CATEGORY == ' ' ] && cat ~/scripts/assets/bookmarks/* |uniq -u |sort
WHERE=$(cat ~/scripts/assets/bookmarks/$CATEGORY |cut -f 1 |$MENUS -i -l 50 -p 'where:')
BROW=$(grep "$WHERE" ~/scripts/assets/bookmarks/$CATEGORY |cut -f 2)
LINK=$(grep "$WHERE" ~/scripts/assets/bookmarks/$CATEGORY |cut -f 3)

# browser select
[ "$BROW" = 'tor' ] && $TORBROWSER $LINK && exit 0
[ "$BROW" = 'clear' ] && $BROWSER $LINK && exit 0
[ "$BROW" = 'i2p' ] && $I2PBROWSER $LINK && exit 0
[ "$BROW" = 'freenet' ] && $FREENETBROWSER $LINK && exit 0
[ "$BROW" = 'zeronet' ] && $ZERONETBROWSER $LINK && exit 0
[ "$BROW" = 'gnunet' ] && $GNUNETBROWSER $LINK && exit 0
[ "$BROW" = 'ipfs' ] && $IPFSBROWSER $LINK && exit 0
[ "$BROW" = 'dat' ] && $DATBROWSER $LINK && exit 0
[ "$BROW" = 'lokinet' ] && $LOKINETBROWSER $LINK && exit 0
